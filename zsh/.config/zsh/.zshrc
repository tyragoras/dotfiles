fpath+=("$ZDOTDIR/functions")

. "$ZDOTDIR/grml-zsh-config"

for f in "$ZDOTDIR/zshrc.d"/*; do
  . "$f"
done
unset f
