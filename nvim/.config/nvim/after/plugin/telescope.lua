local map = require("tyragoras.utils.map")
local builtin = require("telescope.builtin")

require("telescope").setup()

-- keymaps
map("n", "<Leader>ff", function() builtin.find_files() end)
map("n", "<Leader>fg", function() builtin.live_grep() end)
map("n", "<Leader>fb", function() builtin.buffers() end)
map("n", "<Leader>fh", function() builtin.help_tags() end)
