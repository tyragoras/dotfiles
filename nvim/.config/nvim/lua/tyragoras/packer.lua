local autocmd = require("tyragoras.utils.autocmd")

-- bootstrap packer
local packer_bootstrap
local fn = vim.fn
local install_path = fn.stdpath("data").."/site/pack/packer/start/packer.nvim"

if fn.empty(fn.glob(install_path)) > 0 then
  packer_bootstrap = fn.system({"git", "clone", "--depth", "1", "https://github.com/wbthomason/packer.nvim", install_path})
end

-- run PackerSync when plugins.lua file is saved
autocmd("BufWritePost", {
  group = "PackerUserConfig",
  pattern = "plugins.lua",
  command = "source <afile> | PackerSync",
})

if packer_bootstrap then
  require("tyragoras.plugins")
  require("packer").sync()
end
