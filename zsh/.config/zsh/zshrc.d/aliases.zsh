alias s='sudo '
alias e='$EDITOR'

alias ls='ls -hv --color=auto --group-directories-first'
alias ll='ls -l'
alias la='ls -la'
alias cp='cp -vi'
alias mv='mv -vi'
alias rm='trash -v'
alias pd='popd -q'
alias trz='trash-restore'
alias trl='trash-list'
uln() { local f; for f; do unlink "$f"; done; }

alias ngit='nvim +Neogit +tabonly'
