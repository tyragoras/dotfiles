abbr -ag e $EDITOR
abbr -ag s sudo

abbr -ag mv mv -vi
abbr -ag cp cp -vi
abbr -ag rm trash -v --
abbr -ag tl trash-list
abbr -ag tz trash-restore
abbr -ag ngit nvim +Neogit +tabonly

# abbr -ag t 'tmux a || tmux'

function ls
  command ls --color=auto --group-directories-first $argv
end

if string match -r '.*kitty.*' $TERM > /dev/null
  abbr -ag ssh kitty +kitten ssh
end
