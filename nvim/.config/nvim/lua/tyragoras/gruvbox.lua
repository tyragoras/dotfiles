local function no_bg(name)
  vim.api.nvim_set_hl(0, name, {bg = "NONE", ctermbg = "NONE"})
end

vim.g.gruvbox_contrast_dark = "hard"
vim.cmd[[colorscheme gruvbox]]

no_bg("Normal")
-- no_bg("NormalNC")
