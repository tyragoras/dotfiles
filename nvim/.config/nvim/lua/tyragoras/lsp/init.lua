local lspconfig = require("lspconfig")

local servers = {
  "gopls",
  "rust_analyzer",
  "sumneko_lua",
}

local config = {}

config.capabilities = require("cmp_nvim_lsp").update_capabilities(vim.lsp.protocol.make_client_capabilities())

config.on_attach = function(client, bufnr)
  require("tyragoras.lsp.keymaps").setup(bufnr)
end

for _, server in pairs(servers) do
  local has_custom_config, custom_config = pcall(require, "tyragoras.lsp.servers." .. server)

  if has_custom_config then
    config = vim.tbl_deep_extend("force", config, custom_config)
  end

  lspconfig[server].setup(config)
end
