local map = require("tyragoras.utils.map")
vim.g.netrw_liststyle = 3
-- vim.g.netrw_banner = 0
map("n", "<Leader>n", ":Explore<CR>")
