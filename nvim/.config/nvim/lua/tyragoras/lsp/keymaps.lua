local M = {}

M.setup = function(bufnr)
  local map = require("tyragoras.utils.map")
  local map_opts = {buffer = bufnr}

  -- See `:help vim.lsp.*` for documentation on any of the below functions
  map("n", "gD", vim.lsp.buf.declaration, map_opts)
  map("n", "gd", vim.lsp.buf.definition, map_opts)
  map("n", "K", vim.lsp.buf.hover, map_opts)
  map("n", "gi", vim.lsp.buf.implementation, map_opts)
  map({"n", "i"}, "<C-k>", vim.lsp.buf.signature_help, map_opts)
  map("n", "<Space>wa", vim.lsp.buf.add_workspace_folder, map_opts)
  map("n", "<Space>wr", vim.lsp.buf.remove_workspace_folder, map_opts)
  map("n", "<Space>wl", function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, map_opts)
  map("n", "<Space>D", vim.lsp.buf.type_definition, map_opts)
  map("n", "<Space>rn", vim.lsp.buf.rename, map_opts)
  map("n", "<Space>ca", vim.lsp.buf.code_action, map_opts)
  map("n", "gr", vim.lsp.buf.references, map_opts)
  map("n", "<Space>F", vim.lsp.buf.formatting, map_opts)
end

return M
