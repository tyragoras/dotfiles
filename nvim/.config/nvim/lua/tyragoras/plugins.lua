return require("packer").startup(function(use)
  use("wbthomason/packer.nvim")

  use("neovim/nvim-lspconfig")

  use("williamboman/mason.nvim")
  use("williamboman/mason-lspconfig.nvim")

  use({
    "jose-elias-alvarez/null-ls.nvim",
    requires = "nvim-lua/plenary.nvim",
  })

  use("hrsh7th/nvim-cmp")
  use("hrsh7th/cmp-nvim-lsp")
  use("hrsh7th/cmp-buffer")
  use("hrsh7th/cmp-path")
  use("saadparwaiz1/cmp_luasnip")

  use({
    "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
  })
  use("nvim-treesitter/nvim-treesitter-context")

  use("gruvbox-community/gruvbox")
  use("folke/tokyonight.nvim")

  use({
    "nvim-lualine/lualine.nvim",
    requires = {"kyazdani42/nvim-web-devicons", opt = true},
    config = function() require("lualine").setup{} end,
  })

  use({
    "L3MON4D3/LuaSnip",
    requires = {"rafamadriz/friendly-snippets", opt = true},
    config = function() require("luasnip.loaders.from_vscode").lazy_load() end,
  })

  use({
    "nvim-telescope/telescope.nvim",
    requires = "nvim-lua/plenary.nvim",
  })

  use({
    "TimUntersberger/neogit",
    requires = "nvim-lua/plenary.nvim",
  })

  use({
    "lewis6991/gitsigns.nvim",
    config = function() require("gitsigns").setup() end,
  })

  use("mbbill/undotree")

  use({
    "numToStr/Comment.nvim",
    config = function() require("Comment").setup() end,
  })

  use({
    "nvim-orgmode/orgmode",
    ft = {"org"},
    config = function()
      require("orgmode").setup_ts_grammar()
      require("orgmode").setup({
        org_agenda_files = {"~/Sync/org/*"},
      })
    end,
  })
end)
