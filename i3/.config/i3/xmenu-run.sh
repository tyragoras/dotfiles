#!/bin/sh
# vim: ts=8
# TODO: this file is a mess

i3config="$HOME/.config/i3/config"
cache=$XDG_CACHE_HOME/xdg-xmenu/menu

workspace_cmd() {
  awk -F \" '/set \$ws/ { print $(NF-1) }' "$i3config" \
    | while read -r n; do
      printf '\t\t%s' "$n"
      printf '\t%s' "i3-msg $* $n"
      printf '\n'
    done

  unset img
}

mkdir -p "$XDG_CACHE_HOME/xdg-xmenu"

new="$(find /usr/share/applications/mimeinfo.cache \
            "$HOME/.local/share/applications" \
            -newer "$HOME/.cache/xdg-xmenu/menu")"

if [ -n "$new" ] || [ ! -f "$cache" ]; then
  echo "update xdg-xmenu cache"
  xdg-xmenu -G -t "$TERMINAL" | grep -v lsp-plugins > "$cache"
fi

cat "$cache" - <<EOF | xmenu "$@" | sh &

IMG:/usr/share/icons/hicolor/48x48/apps/org.xfce.thunar.png		Thunar		thunar
IMG:/usr/share/icons/Adwaita/32x32/legacy/utilities-terminal.png	Terminal	$TERMINAL
IMG:/usr/share/icons/hicolor/32x32/apps/firefox-esr.png			Firefox		firefox -P
IMG:/home/ting/.cache/xdg-xmenu/icons/mpv.png				mpv		clip-run mpv
IMG:/home/ting/.cache/xdg-xmenu/icons/mpv.png				mpvq		clip-run mpvq

IMG:/usr/share/icons/Adwaita/24x24/legacy/preferences-system-windows.png		i3
	IMG:/usr/share/icons/Adwaita/24x24/legacy/preferences-system-windows.png	goto workspace
$(workspace_cmd workspace number)
	IMG:/usr/share/icons/Adwaita/24x24/legacy/preferences-system-windows.png	move window to workspace
$(workspace_cmd move container to workspace number)
	IMG:/usr/share/icons/Adwaita/24x24/legacy/preferences-system-windows.png	change layout
		fullscreen		i3-msg fullscreen
		stacking		i3-msg layout stacking
		tabbed			i3-msg layout tabbed
		toggle floating		i3-msg floating toggle
		toggle split		i3-msg layout toggle split
	IMG:/usr/share/icons/Adwaita/24x24/legacy/window-close.png			kill window	i3-msg kill
EOF
