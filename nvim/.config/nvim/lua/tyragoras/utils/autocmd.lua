return function(event, opts)
  if type(opts.group) == "string" then
    opts.group = vim.api.nvim_create_augroup(opts.group, {})
  end
  vim.api.nvim_create_autocmd(event, opts)
end
