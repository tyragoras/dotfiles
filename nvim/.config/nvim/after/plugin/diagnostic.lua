local map = require("tyragoras.utils.map")

map("n", "<Space>cc", vim.diagnostic.open_float)
map("n", "<Space>cp", vim.diagnostic.goto_prev)
map("n", "<Space>cn", vim.diagnostic.goto_next)
