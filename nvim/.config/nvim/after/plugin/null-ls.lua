local builtins = require("null-ls.builtins")

require("null-ls").setup({
  sources = {
    builtins.code_actions.shellcheck,
    builtins.diagnostics.shellcheck,
  },
  on_attach = function()
    require("tyragoras.lsp.keymaps").setup(0)
  end,
})
