local map = require("tyragoras.utils.map")
local neogit = require("neogit")

neogit.setup()

map("n", "<Leader>gs", neogit.open)
