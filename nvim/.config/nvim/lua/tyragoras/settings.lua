local g = vim.g
local o = vim.o
-- local opt = vim.opt

g.mapleader = " "
g.maplocalleader = " "

o.colorcolumn = "81"
o.list = true
o.number = true
o.relativenumber = true
o.scrolloff = 8
o.showmode = false
o.undofile = true
o.wrap = false

-- opt.clipboard:append("unnamedplus")
