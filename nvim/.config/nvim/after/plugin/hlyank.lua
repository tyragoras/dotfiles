local autocmd = require("tyragoras.utils.autocmd")

autocmd("TextYankPost", {
  group = "HLYank",
  pattern = "*",
  callback = function()
    vim.highlight.on_yank{
      timeout = 40,
    }
  end,
})
