addpath() {
  case :$PATH: in
    :$1:) return 1 ;;
  esac
  case $1 in
    -) export PATH="${PATH:+$PATH:}$2" ;; # append
    *) export PATH="$1${PATH:+:$PATH}" ;; # prepend
  esac
}

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

export ZDOTDIR="$XDG_CONFIG_HOME/zsh"

export EDITOR="nvim"
export TERMINAL="xfce4-terminal"

export GOPATH="$XDG_DATA_HOME/go"
addpath "$HOME/.local/opt/go/bin"
addpath "$GOPATH/bin"

export N_PREFIX="$HOME/.local/opt/node"
addpath "$N_PREFIX/bin"

export CARGO_HOME="$XDG_DATA_HOME/cargo"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
. "$CARGO_HOME/env"

addpath "$HOME/.local/opt/appimage"
addpath "$HOME/.local/bin"
addpath "$HOME/.local/sbin"

export LESSHISTFILE="$XDG_CACHE_HOME/lesshst"
export SUDO_ASKPASS="$HOME/.local/sbin/askpass"

export QT_QPA_PLATFORMTHEME="qt5ct"

if [ -z "$DISPLAY" ] && [ "$(tty)" = /dev/tty1 ]; then
  exec startx
fi
