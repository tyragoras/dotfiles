src_dir="$HOME/Projects"

gcl() {
  local d
  d="${${1#*://}%.git}"     # remove '*://' and '.git'
  d="${d%/*}/${${d##*/}#.}" # remove '.' at beginning of repo
  d="$src_dir/$d/.git"
  git clone --bare "$1" "$d"
}
