require("nvim-treesitter.configs").setup({
  ensure_installed = {
    "bash",
    "fish",
    "go",
    "html",
    "lua",
    "markdown",
    "org",
    "rust",
    "toml",
    "yaml",
  },
  -- auto_install = true,
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
  },
  -- indent = { enable = true },
})
