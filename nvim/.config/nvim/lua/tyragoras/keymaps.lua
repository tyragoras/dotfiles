local map = require("tyragoras.utils.map")

map("x", "J", ":m '>+1<CR>gv=gv")
map("x", "K", ":m '<-2<CR>gv=gv")

map("n", "<C-c>", "<Esc><Cmd>silent nohlsearch<CR>")
map("i", "<C-c>", "<Esc>")

map({"n", "x"}, "<Leader>y", '"+y')
map({"n", "x"}, "<Leader>Y", '"+Y')

map({"n", "x"}, "<Leader>d", '"_d')

map("n", "<Leader>s", ":%s//gc<Left><Left><Left>")
map("n", "<Leader>ds", ":%s/\\s\\+$//e<CR>")

map("n", "Q", "@q") -- replay macro 'q'
