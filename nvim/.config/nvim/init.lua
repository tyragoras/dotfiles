require("tyragoras.utils.globals")

require("tyragoras.settings")
require("tyragoras.keymaps")

require("tyragoras.packer")
require("tyragoras.plugins")

-- require("tyragoras.gruvbox")
require("tyragoras.tokyonight")

require("tyragoras.mason") -- always before lsp
require("tyragoras.lsp")
